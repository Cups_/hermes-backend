using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hermes
{
    [Serializable]
    public class Packet
    {
        public string CommandName = "";
        public object[] CommandArguments = null;
        public byte[] PacketIV = null;
        public string AuthorizationToken = "";

        /// <summary>
        /// Repr�sente l'envoyeur du packet, peut �tre null et ne doit etre seulement utilis� qu'une fois le packet en boucle locale arriveur.
        /// </summary>
        public string CommandSender = "";

        public Packet(string CommandName, object[] CommandArguments)
        {
            this.CommandName = CommandName;
            this.CommandArguments = CommandArguments;
        }

        public Packet(string CommandName, object CommandArgument)
        {
            this.CommandArguments = new object[]
            {
                CommandArgument
            };
            this.CommandName = CommandName;
        }

        public Packet SetIV(byte[] IV)
        {
            this.PacketIV = IV;
            return this;
        }

        public Packet SetAuthorizationToken(string AuthorizationToken)
        {
            this.AuthorizationToken = AuthorizationToken;
            return this;
        }

        public Packet SetCommandName(string commandName)
        {
            this.CommandName = commandName;
            return this;
        }

        public Packet SetCommandArguments(object[] CommandArguments)
        {
            this.CommandArguments = CommandArguments;
            return this;
        }
    }
}
