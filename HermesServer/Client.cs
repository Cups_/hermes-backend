/*
This source code is part of the Hermes project, a free open source encrypted messaging program made by Al-Yakoob Fourat and Prost Julien.
    Copyright (C) 2019  Al-Yakoob - Prost

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using Hermes;
using HermesServer.Classes;
using HermesServer.RSAImplementation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;

using static HermesServer.Global;

namespace HermesServer
{
    /// <summary>
    /// Cette classe permet d'encapsuler l'object client ainsi que son socket de connexion.
    /// </summary>
    public class Client
    {
        /// <summary>
        /// Référence pointant a la connexion TCP du client.
        /// </summary>
        public TcpClient TcpConnection { get; private set; }

        /// <summary>
        /// Indice permettant de retrouver le client.
        /// </summary>
        public string UID;  

        /// <summary>
        /// Le token du client si il est connecté.
        /// </summary>
        public string TokenID;

        /// <summary>
        /// Classe permettant l'encryption et la décryption des messages.
        /// </summary>
        public DiffieHellman diffieHellman = null;

        /// <summary>
        /// La clé publique du client.
        /// </summary>
        private byte[] ClientPublicKey = null;

        /// <summary>
        /// L'IV du client.
        /// </summary>
        private byte[] ClientIV = null;

        /// <summary>
        /// Le nom d'utilisateur du client.
        /// </summary>
        public string ClientName = "";

        /// <summary>
        /// Une liste de tout les packets arrivés et non traités, il fonctionne de façon FIFO, First In First Out, le premier paquet arrivé est traité en priorité.
        /// </summary>
        public List<Packet> PacketToTreat = new List<Packet>();

        /// <summary>
        /// Une liste contenant le nom d'utilisateur de tout ceux ayant effectués un échange de clé avec le client actuel.
        /// </summary>
        public List<string> LinkedUsers = new List<string>();

        /// <summary>
        /// Un objet utilisé pour garantir que deux threads n'accédent pas a la liste de packets en même temps.
        /// </summary>
        private object Lock = new object();

        public Thread ClientCommunicationThread = null;

        public Thread MessageManagementThread = null;

        public Thread MessageRoutineThread = null;

        public Client(string UID)
        {

            this.TcpConnection = server.TcpServer.AcceptTcpClient();

            Console.WriteLine("[Log] Nouvelle connexion de : " + TcpConnection.Client.RemoteEndPoint.ToString());

            this.UID = UID;

            this.diffieHellman = new DiffieHellman();

            
            this.MessageRoutineThread = new Thread(MessageRoutine);
            this.MessageRoutineThread.Name = "Thread routine message pour " + UID;
            this.MessageRoutineThread.Start();
            
        }

        public void SendPacket(Packet packet)
        {
            byte[] array = diffieHellman.Encrypt(ClientPublicKey, packet);

            TcpConnection.GetStream().Write(BitConverter.GetBytes(array.Length), 0, sizeof(Int32));
            TcpConnection.GetStream().Write(diffieHellman.IV, 0, 16);
            TcpConnection.GetStream().Write(array, 0, array.Length);
        }

        private int CheckSum(byte[] array)
        {
            int checksum = 0;

            foreach (byte item in array)
            {
                checksum += item;
            }

            return checksum;
        }

        


        public Packet ReceivePacket()
        {
            // On crée un tableau capable de contenir un nombre Int32 (32 bit soit 32/8 = 4 octets).
            byte[] sizePacket = BitConverter.GetBytes(0);

            // On récupère la taille du packet en octets.
            TcpConnection.Client.Receive(sizePacket, sizePacket.Length, SocketFlags.None);

            // On "converti" ces octets en Int32 (entier)
            int sp = BitConverter.ToInt32(sizePacket, 0); // sp = SizePacket.

            // On récupère l'IV
            TcpConnection.Client.Receive(ClientIV, SocketFlags.None);

            // On récupère (sp) octets, ces octets sont encryptés.
            byte[] encryptedBytes = new byte[sp];
            TcpConnection.Client.Receive(encryptedBytes, encryptedBytes.Length, SocketFlags.None);

            int checksum = CheckSum(encryptedBytes);

            Console.WriteLine("GOT MESSAGE " + UID + "  " + checksum + " " + CheckSum(ClientIV) + " " + CheckSum(ClientPublicKey) + " " + CheckSum(diffieHellman.PublicKey) + " " + CheckSum(diffieHellman.IV));

            // On décrypte ces octets et on les convertis en packet.
           

            Packet returnPacket = diffieHellman.Decrypt(ClientPublicKey, encryptedBytes, ClientIV);

            if (returnPacket.CommandName == "PACKET_TRANSFER")
            {
                returnPacket.SetIV((byte[])returnPacket.CommandArguments[2]);
            }


            return returnPacket;
        }

        /// <summary>
        /// Cette boucle va servir de "centre d'arrivage", elle va s'occuper de récuperer et transmettre les packets entrants.
        /// </summary>
        public void ClientCommunicationLoop()
        {
            while (TcpConnection.Connected)
            {
                try
                {
                    RETRY:
                    // On ajoute le packet reçu à la liste des packets à traiter.
                    Packet packetToAdd = ReceivePacket();

                    if (packetToAdd == null) goto RETRY;

                    lock (Lock)
                    {
                        PacketToTreat.Add(packetToAdd);
                    }
                   

                    // On a reçu un packet, on reset le timeout du client, si il est connecté.
                    if (TokenID != null)
                        UserTokens[TokenID] = (UserTokens[TokenID].Item1, new DateTime());
                }
                catch (Exception ex)
                {
                    try
                    {
                        Console.WriteLine("[Error] Failed to receive packets, closing connection from " + UID + " from " + TcpConnection.Client.RemoteEndPoint.ToString());
                        TcpConnection.Close();
                    }
                    catch (Exception)
                    {

                     
                    }
                    
                    clients.Remove(this);
                }         
            }
        }

        /// <summary>
        /// Permet la déconnexion propre d'un utilisateur.
        /// </summary>
        public void UserDisconnect()
        {
            try
            {
                TcpConnection.Close();
                clients.Remove(this);
            }
            catch (Exception)
            {
            }        
        }

        /// <summary>
        /// Cette boucle traite certains packets généraux.
        /// </summary>
        public void PacketManagementLoop()
        {
            // Tant que l'on est connecté ...
            while (TcpConnection.Connected)
            {
                // ... et qu'il y a un packet à traiter.
                lock (Lock)
                {
                    if (PacketToTreat.Count != 0)
                    {

                        try
                        {
                            // On récupere le premier packet de la liste, correspondant au packet le plus vieux (FIFO)
                            Packet packetToTreat = PacketToTreat[0];

                            // Et on le retire de la liste vu que l'on va le traiter.
                            PacketToTreat.Remove(packetToTreat);

                            switch (packetToTreat.CommandName)
                            {
                                case "USER_LOGIN":
                                    SendPacket(server.UserLogin((string)packetToTreat.CommandArguments[0], this));

                                    SendPacket(new Packet("MESSAGE_RECEIVED", new object[] { new MessageObject("Server", "Hella Gay", DateTime.Now) })); // Test Packet

                                    break;
                                case "USER_REGISTER":
                                    // On register le nouveau client et on envoi le paquet de réponse.
                                    SendPacket(server.UserRegister((string)packetToTreat.CommandArguments[0]));
                                    break;
                                case "CONNECTION_LINK_REQUEST":
                                    // Packet demandant la mise en relation de deux client, voir notes du read me du serveur.
                                    ConnectionLinkObject clObject = (ConnectionLinkObject)packetToTreat.CommandArguments[0];

                                    // On execute la liaison.
                                    server.ConnectionLink(packetToTreat, this);
                                    break;
                                case "CONNECTION_LINK_CHECK":
                                    // Packet vérifiant la liaison entre deux utilisateurs.
                                    SendPacket(new Packet("CONNECTION_LINK_CHECK_ANSWER", new object[] { LinkedUsers.Contains((string)packetToTreat.CommandArguments[0]) })); // On répond avec un simple booléen vérifiant si le nom est contenu dans la liste LinkedUsers.
                                    break;
                                case "USER_LIST":
                                    SendPacket(server.UserList());
                                    break;
                                case "PACKET_TRANSFER":
                                    server.PacketTransfer(packetToTreat, this);
                                    break;
                                case "USER_UPDATE_AVATAR":
                                    SendPacket(server.UserUpdateAvatar(packetToTreat, this));
                                    break;
                                case "USER_DISCONNECT":
                                    UserDisconnect();
                                    break;
                                default:
                                    // Ce n'est pas un packet qui doit être traité ici, on le laisse donc.
                                    PacketToTreat.Add(packetToTreat);
                                    break;

                            }
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("[Log] Erreur lors de la lecture des messages clients, fermeture de la connexion.");
                            TcpConnection.Close();
                        }
                    }
                }

                Thread.Sleep(5); // Après analyse au profiler, le loop force l'utilisation du CPU, on sleep à chaque loop pour réduire l'impact.
            }
        }

        /// <summary>
        /// Permet de récuperer un paquet en le retirant de la liste.
        /// </summary>
        /// <param name="filters">Filtre permettant le tri en se basant sur le commandName du packet.</param>
        /// <returns></returns>
        public Packet PopPacket(string[] filters)
        {
            DateTime start = DateTime.Now;

            if (filters.Length == 0)
            {
                // On ne souhaite pas un paquet en particulier.
                lock (Lock)
                {
                    Packet returnPacket;

                    if (PacketToTreat.Count != 0)
                    {
                        returnPacket = PacketToTreat[0];
                        PacketToTreat.Remove(returnPacket);
                        return returnPacket;
                    }
                    else
                    {
                        return null;
                    }


                }
            }
            else
            {
                lock (Lock)
                {
                    foreach (Packet packet in PacketToTreat)
                    {
                        foreach (string filter in filters)
                        {
                            if (packet.CommandName.Contains(filter))
                            {
                                PacketToTreat.Remove(packet);
                                return packet;
                            }
                        }
                    }

                   
                }

                return null;
            }
        }

        public void MessageRoutine()
        {
            // On envoie notre clé publique de 140 charactères.
            TcpConnection.GetStream().Write(diffieHellman.PublicKey, 0, diffieHellman.PublicKey.Length);

            // Et on récupere celle du client
            ClientPublicKey = new byte[140];
            TcpConnection.GetStream().Read(ClientPublicKey, 0, ClientPublicKey.Length);

            TcpConnection.GetStream().Write(diffieHellman.IV, 0, diffieHellman.IV.Length);

            // Et on récupere celle du client
            ClientIV = new byte[16];
            TcpConnection.GetStream().Read(ClientIV, 0, 16);

            
            // On communique maintenant en packet, on démarre donc le centre d'arrivage.
            ClientCommunicationThread = new Thread(ClientCommunicationLoop);
            ClientCommunicationThread.Name = "Thread d'écoute " + UID;
            ClientCommunicationThread.Start();

            // Et on démarre le thread les gérant.
            MessageManagementThread = new Thread(PacketManagementLoop);
            MessageManagementThread.Name = "Thread de management de packet" + UID;
            MessageManagementThread.Start();
            
            
        }
    }
}
