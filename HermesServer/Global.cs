/*
This source code is part of the Hermes project, a free open source encrypted messaging program made by Al-Yakoob Fourat and Prost Julien.
    Copyright (C) 2019  Al-Yakoob - Prost

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using HermesServer.RSAImplementation;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace HermesServer
{
    public class Global
    {
        /// <summary>
        /// La liste dynamique des clientsc connectés
        /// </summary>
        public static List<Client> clients = new List<Client>();

        /// <summary>
        /// The server.
        /// </summary>
        public static Server server = null;

        /// <summary>
        /// Ce dictionnaire contient les tokens utilsateurs de la sorte TOKENID, (USERNAME, TEMPSDEPUISACTIVITE)
        /// </summary>
        public static Dictionary<string, (string, DateTime)> UserTokens = new Dictionary<string, (string, DateTime)>();
    }
}
