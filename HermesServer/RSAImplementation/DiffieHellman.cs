﻿using Hermes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace HermesServer.RSAImplementation
{
    public class DiffieHellman
    {
        private Aes aes = null;
        private ECDiffieHellmanCng dfh = null;

        private readonly byte[] publicKey;

        public byte[] PublicKey
        {
            get
            {
                return this.publicKey;
            }
        }

        public byte[] IV
        {
            get
            {
                return this.aes.IV;
            }
        }

        public byte[] Encrypt(byte[] publicKey, Packet packet)
        {
            byte[] encryptedMessage;
            var key = CngKey.Import(publicKey, CngKeyBlobFormat.EccPublicBlob);
            var derivedKey = this.dfh.DeriveKeyMaterial(key); // "Common secret"

            this.aes.Key = derivedKey;
            this.aes.Padding = PaddingMode.Zeros;

            BinaryFormatter bf = new BinaryFormatter();
            bf.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;

            using (var cipherText = new MemoryStream())
            {
                using (var encryptor = this.aes.CreateEncryptor())
                {
                    using (var cryptoStream = new CryptoStream(cipherText, encryptor, CryptoStreamMode.Write))
                    {
                        bf.Serialize(cryptoStream, packet);
                    }
                }

                encryptedMessage = cipherText.ToArray();
            }

            return encryptedMessage;
        }

        public byte[] Encrypt(byte[] publicKey, byte[] data)
        {
            byte[] encryptedMessage;
            var key = CngKey.Import(publicKey, CngKeyBlobFormat.EccPublicBlob);
            var derivedKey = this.dfh.DeriveKeyMaterial(key); // "Common secret"

            this.aes.Key = derivedKey;
            this.aes.Padding = PaddingMode.Zeros;
            BinaryFormatter bf = new BinaryFormatter();

            using (var cipherText = new MemoryStream())
            {
                using (var encryptor = this.aes.CreateEncryptor())
                {
                    using (var cryptoStream = new CryptoStream(cipherText, encryptor, CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(data, 0, data.Length);
                    }
                }

                encryptedMessage = cipherText.ToArray();
            }

            return encryptedMessage;
        }

        public static byte[] TrimEnd(byte[] array)
        {
            int lastIndex = Array.FindLastIndex(array, b => b != 0);

            Array.Resize(ref array, lastIndex + 1);

            return array;
        }

        public Packet Decrypt(byte[] publicKey, byte[] encryptedMessage, byte[] iv)
        {
            using (MemoryStream sr = new MemoryStream(DecryptBytes(publicKey, encryptedMessage, iv)))
            {
                var binFormat = new BinaryFormatter();
                binFormat.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
                binFormat.Binder = new AllowAllAssemblyVersionsDeserializationBinder();

                return (Packet)binFormat.Deserialize(sr);
            }
            
            /*
            var key = CngKey.Import(publicKey, CngKeyBlobFormat.EccPublicBlob);
            var derivedKey = this.diffieHellman.DeriveKeyMaterial(key);

            this.aes.Key = derivedKey;
            this.aes.IV = TrimEnd(iv);
            this.aes.Padding = PaddingMode.Zeros;

            byte[] returnBuffer = new byte[4096];
            using (var plainText = new MemoryStream())
            {
                using (var decryptor = this.aes.CreateDecryptor())
                {
                    using (var cryptoStream = new CryptoStream(plainText, decryptor, CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(encryptedMessage, 0, encryptedMessage.Length);
                    }
                }

                 returnBuffer = plainText.GetBuffer();

            }

            using (MemoryStream sr = new MemoryStream(returnBuffer))
            {
                var binFormat = new BinaryFormatter();
                binFormat.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
                binFormat.Binder = new AllowAllAssemblyVersionsDeserializationBinder();
                return (Packet)binFormat.Deserialize(sr);
            }*/
        }

        public byte[] DecryptBytes(byte[] publicKey, byte[] encryptedMessage, byte[] iv)
        {

            var key = CngKey.Import(publicKey, CngKeyBlobFormat.EccPublicBlob);
            var derivedKey = this.dfh.DeriveKeyMaterial(key);

            this.aes.Key = derivedKey;
            this.aes.IV = iv;
            this.aes.Padding = PaddingMode.Zeros;

            using (var plainText = new MemoryStream())
            {
                using (var decryptor = this.aes.CreateDecryptor())
                {
                    using (var cryptoStream = new CryptoStream(plainText, decryptor, CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(encryptedMessage, 0, encryptedMessage.Length);
                    }
                }

                return plainText.GetBuffer();
            }
        }

        public DiffieHellman()
        {
            this.aes = new AesCryptoServiceProvider();

            this.dfh = new ECDiffieHellmanCng
            {
                KeyDerivationFunction = ECDiffieHellmanKeyDerivationFunction.Hash,
                HashAlgorithm = CngAlgorithm.Sha256
            };

            // This is the public key we will send to the other party
            this.publicKey = this.dfh.PublicKey.ToByteArray();
        }
    }

    sealed class AllowAllAssemblyVersionsDeserializationBinder : System.Runtime.Serialization.SerializationBinder
    {
        public override Type BindToType(string assemblyName, string typeName)
        {
            Type typeToDeserialize = null;

            String currentAssembly = Assembly.GetExecutingAssembly().FullName;

            // In this case we are always using the current assembly
            assemblyName = currentAssembly;

            // Get the type using the typeName and assemblyName
            typeToDeserialize = Type.GetType(String.Format("{0}, {1}",
                typeName, assemblyName));

            return typeToDeserialize;
        }
    }
}
