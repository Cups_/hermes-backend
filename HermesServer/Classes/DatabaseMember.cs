﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HermesServer.Classes
{
    public class DatabaseMember
    {
        /// <summary>
        /// L'ID Database de l'utilisateur.
        /// </summary>
        public MongoDB.Bson.ObjectId Id { get; set; }

        /// <summary>
        /// Le nom d'utilisateur du membre
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Le mot de passe hashé en Argon2i du membre
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// L'avatar du client, en base64.
        /// </summary>
        public string AvatarBase64 { get; set; }

        [BsonConstructor]
        public DatabaseMember(string Username, string Password, string AvatarBase64)
        {
            this.Username = Username;
            this.Password = Password;
            this.AvatarBase64 = AvatarBase64;
        }
    }
}
