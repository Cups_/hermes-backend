﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hermes
{
    [Serializable]
    public class UserListObject
    {
        /// <summary>
        /// Tout les utilisateurs inscrits.
        /// </summary>
        public UserObject[] AllUsers = null;

        /// <summary>
        /// Tout les utilisateurs connectés.
        /// </summary>
        public UserObject[] ConnectedUsers = null;

        public UserListObject(UserObject[] AllUsers, UserObject[] ConnectedUsers)
        {
            this.AllUsers = AllUsers;
            this.ConnectedUsers = ConnectedUsers;
        }
    }
}
