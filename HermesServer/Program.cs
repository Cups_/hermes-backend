/*
This source code is part of the Hermes project, a free open source encrypted messaging program made by Al-Yakoob Fourat and Prost Julien.
    Copyright (C) 2019  Al-Yakoob - Prost

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using static HermesServer.Global;

namespace HermesServer
{
    class Program
    {
        private const int port = 8080;

        static void Main(string[] args)
        {
            Console.Title = "Hermes Server";

            server = new Server(port);
            server.StartServer();


            while (true)
            {
                Thread.Sleep(2);
            }
        }
    }
}
