**Introduction **

Hermes est une messagerie encryptée End To End, elle est faite en sorte à ce que aucune des données transitante ne soit lisible par une personne étrangère ou par le serveur lui même.

La base de données Hermes est mongodb, voila pourquoi : 
*  Libre d'utilisation et gratuite.
*  Simple à implémenter.
*  Rapide et plus sécurisée qu'une alternative SQL (SQLi...)

L'encryption utilisée est une encryption AES basique, à voir si on doit la modifier.


[NOTES]

- [1] Afin de garantir la transparence du serveur, on fera en sorte de servir de "middleman" et d'utiliser les caractéristiques de l'échange de clé diffie helman afin 
garantir la notion d'encryption end to end, (voir commande "CONNECTION_LINK")
